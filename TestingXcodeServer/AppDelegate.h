//
//  AppDelegate.h
//  TestingXcodeServer
//
//  Created by Ankit Gupta on 7/31/14.
//  Copyright (c) 2014 R Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
